//
//  LoginViewController.swift
//  Coinz
//
//  Created by Paulo Camopy on 01/02/17.
//  Copyright © 2017 Smart Apps. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    let login_url = "http://www.kaleidosblog.com/tutorial/login/api/Login"
    
    var login_session:String = ""
    
    @IBOutlet weak var emailInput: UITextField!
    @IBOutlet weak var passwordInput: UITextField!
    @IBOutlet weak var loginActivityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func doLogin(_ sender: Any) {
        if(emailInput.text == "" || passwordInput.text == ""){
            return
        }
        
        loginActivityIndicator.startAnimating()
        let post_data: NSDictionary = NSMutableDictionary()
        
        post_data.setValue(emailInput.text, forKey: "username")
        post_data.setValue(passwordInput.text, forKey: "password")
        
        let url:URL = URL(string: login_url)!
        let session = URLSession.shared
        
        let request = NSMutableURLRequest(url: url)
        request.httpMethod = "POST"
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
        
        var paramString = ""
        
        
        for (key, value) in post_data
        {
            paramString = paramString + (key as! String) + "=" + (value as! String) + "&"
        }
        
        request.httpBody = paramString.data(using: String.Encoding.utf8)
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: {
            (
            data, response, error) in
            
            guard let _:Data = data, let _:URLResponse = response  , error == nil else {
                return
            }
            
            let json: Any?
            
            do
            {
                json = try JSONSerialization.jsonObject(with: data!, options: [])
            }
            catch
            {
                return
            }
            
            guard let server_response = json as? NSDictionary else
            {
                return
            }
            
            if let data_block = server_response["data"] as? NSDictionary
            {
                if let session_data = data_block["session"] as? String
                {
                    self.login_session = session_data
                    
                    let preferences = UserDefaults.standard
                    preferences.set(session_data, forKey: "session")
                    
                    self.loginActivityIndicator.stopAnimating()
                    DispatchQueue.main.async(execute: self.loginDone)
                }
            }
            else{
                DispatchQueue.main.async(execute: self.loginIncorrect)
            }
        })
        
        task.resume()
    }
    
    func loginIncorrect(){
        loginActivityIndicator.stopAnimating()
        // create the alert
        let alert = UIAlertController(title: "Login inválido", message: "E-mail ou senha incorretos", preferredStyle: UIAlertControllerStyle.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    func loginDone()
    {
        let storyboard = UIStoryboard(name: "Wallet", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "WalletViewController") as! WalletViewController
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        //show window
        appDelegate.window?.rootViewController = controller
    }
    @IBAction func recoverPassword(_ sender: Any) {
        
        let titlePrompt = UIAlertController(title: "Digite seu email de cadastrado",
                                            message: nil,
                                            preferredStyle: .alert)
        
        var titleTextField: UITextField?
        titlePrompt.addTextField {
            (textField) -> Void in
            titleTextField = textField
            textField.placeholder = "E-mail"
            textField.autocorrectionType = .no
        }
        
        titlePrompt.addAction(UIAlertAction(title: "Cancelar",
                                            style: .default,
                                            handler: {return nil}()))
        
        titlePrompt.addAction(UIAlertAction(title: "Confirmar",
                                            style: .default,
                                            handler: { (action) -> Void in
                                                if let textField = titleTextField {
                                                    self.sendPasswordRecoverEmail(textField.text!)
                                                }
        }))
        
        self.present(titlePrompt,
                     animated: true,
                     completion: nil)

    }
    
    func sendPasswordRecoverEmail(_ email: String){
        
    }
}
